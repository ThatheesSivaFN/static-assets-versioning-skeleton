module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.config.init({
        config: {
            srcDirectoryPath: "./src/",
            distDirectoryPath: "./dist/",
            tempDirectoryPath: './.tmp/'
        },
        useminPrepare: {
            html: '<%= config.srcDirectoryPath %>*.html',
            options: {
                dest: '<%= config.distDirectoryPath %>'
            }
        },
        usemin: {
            html: ['<%= config.distDirectoryPath %>*.html'],
            css: ['<%= config.distDirectoryPath %>css/**/*.css']
        },
        copy: {
            html: {
                expand: true,
                cwd: "<%= config.srcDirectoryPath %>",
                src: ['**/**.html'],
                dest: '<%= config.distDirectoryPath %>'
            },
            js: {
                expand: true,
                cwd: "<%= config.srcDirectoryPath %>js",
                src: ['**/**'],
                dest: '<%= config.distDirectoryPath %>js'
            },
            css: {
                expand: true,
                cwd: "<%= config.srcDirectoryPath %>css",
                src: ['**/**'],
                dest: '<%= config.distDirectoryPath %>css'
            },
            img: {
                expand: true,
                cwd: "<%= config.srcDirectoryPath %>img",
                src: ['**/**'],
                dest: '<%= config.distDirectoryPath %>img'
            }
        },
        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 8
            },
            source: {
                files: [{
                    src: [
                        '<%= config.distDirectoryPath %>js/**/*.js',
                        '<%= config.distDirectoryPath %>css/**/*.css',
                        '<%= config.distDirectoryPath %>img/**/*.*'
                    ]
                }]
            }
        },
        clean: {
            dist: ["<%= config.distDirectoryPath %>**/**"],
            tmp: ["<%= config.tempDirectoryPath %>**/**"]
        }

    });

    grunt.registerTask('dev', [
        'newer:copy:html',
        'newer:copy:js',
        'newer:copy:css',
        'newer:copy:img'
    ]);

    grunt.registerTask('staging', [
        'clean',
        'copy:html',
        'useminPrepare',
        'concat',
        'uglify',
        'cssmin',
        'copy:img',
        'filerev',
        'usemin'
    ]);

    grunt.registerTask('prod', [
        'clean',
        'copy:html',
        'useminPrepare',
        'concat',
        'uglify',
        'cssmin',
        'copy:img',
        'filerev',
        'usemin'
    ]);

    grunt.registerTask('default', 'dev');
}
