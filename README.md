# Static Assets Versioning Skeleton #

It uses GruntJS as task runner for the automatic static assets(js, css & images) versioning. The same technique is applicable for other JavaScript task runners too.

### Compile Source files ###

Dev Environment:

```
grunt dev
```

Staging Environment:

```
grunt staging
```

Production Environment:

```
grunt prod
```

### Generated 'dist' directory structure ###
```
dist
├── about.html
├── css
│   └── style.6f95ddb4.css
├── img
│   └── star.72519226.png
├── index.html
└── js
    ├── about.06b1c6ee.js
    ├── home.c4890a64.js
    └── lib
        └── vender
            └── jquery.2ce2f7cf.js
```

### Project Directory Structure ###

```
static-assets-versioning-skeleton
├── README.md
├── dist
│   ├── about.html
│   ├── css
│   │   └── style.6f95ddb4.css
│   ├── img
│   │   └── star.72519226.png
│   ├── index.html
│   └── js
│       ├── about.06b1c6ee.js
│       ├── home.c4890a64.js
│       └── lib
│           └── vender
│               └── jquery.2ce2f7cf.js
├── gruntfile.js
├── package.json
└── src
    ├── about.html
    ├── css
    │   ├── style.css
    │   └── vender
    ├── img
    │   └── star.png
    ├── index.html
    └── js
        ├── about.js
        ├── config.js
        ├── home.js
        └── lib
            ├── cookie.js
            ├── validate.js
            └── vender
                └── jquery-1.11.3.js
```

### TODOs ###

1. Add tasks for TDD 
2. Add tasks for performance improvements (eg: CSS sprite, etc)
3. Create skeletons using other JavaScript task runners